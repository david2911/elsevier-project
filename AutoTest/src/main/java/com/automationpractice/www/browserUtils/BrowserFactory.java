package com.automationpractice.www.browserUtils;

import com.automationpractice.www.utilities.Props;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class BrowserFactory {

    public static WebDriver driver;

    public static WebDriver getDriver(){
        return driver;
    }
    public WebDriver initialiseBrowser(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
//            options.addArguments("--headless");
        options.addArguments("--incognito");
        options.addArguments("--disable-infobars");
        options.addArguments("--enable-javascript");
        driver = new ChromeDriver(options);
        return driver;
    }

    public void prepareBrowser(){
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        int globalTime = Integer.parseInt(Props.getData("browser.time"));
        driver.manage().timeouts().implicitlyWait(globalTime, TimeUnit.MICROSECONDS);
    }

    public void deInitialiseBrowser(){
        if (driver != null){
            driver.quit();
        }
    }
}
