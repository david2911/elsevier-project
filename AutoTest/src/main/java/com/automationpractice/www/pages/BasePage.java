package com.automationpractice.www.pages;

import com.automationpractice.www.utilities.Props;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    protected final WebDriver Driver;

    public BasePage(WebDriver driver){
        this.Driver = driver;
    }
    public HomePage loadHomePage(){
        Driver.navigate().to(Props.getData("base.url"));
        return PageFactory.initElements(Driver, HomePage.class);
    }
}
