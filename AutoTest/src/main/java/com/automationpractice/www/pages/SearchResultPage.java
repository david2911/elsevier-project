package com.automationpractice.www.pages;

import com.automationpractice.www.utilities.Props;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class SearchResultPage extends BasePage {

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public SearchItemPage previewItem() {
        String clickItemScript = "return document.querySelector('div > a.product_img_link > img').click();";
        JavascriptExecutor js = (JavascriptExecutor) Driver;
        js.executeScript(clickItemScript);
        Driver.switchTo().activeElement();
        int timeOut = Integer.parseInt(Props.getData("browser.time"));
        Driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
        return PageFactory.initElements(Driver, SearchItemPage.class);
    }
}
