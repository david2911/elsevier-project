package com.automationpractice.www.pages;

import org.openqa.selenium.WebDriver;

public class UserAuthenticationPage extends BasePage {
    public UserAuthenticationPage(WebDriver driver) {
        super(driver);
    }

    public boolean verifySignInPage(String signIn) {
        return Driver.getPageSource().contains(signIn);
    }
}
