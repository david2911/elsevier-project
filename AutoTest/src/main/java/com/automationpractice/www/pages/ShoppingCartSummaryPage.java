package com.automationpractice.www.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartSummaryPage extends BasePage {
    public ShoppingCartSummaryPage(WebDriver driver) {
        super(driver);
    }

    public UserAuthenticationPage doCheckout() {
        String checkoutScript = "return document.getElementById('center_column').getElementsByClassName('standard-checkout')[0].click();";
        JavascriptExecutor js = (JavascriptExecutor) Driver;
        js.executeScript(checkoutScript);
        return PageFactory.initElements(Driver, UserAuthenticationPage.class);
    }
}
