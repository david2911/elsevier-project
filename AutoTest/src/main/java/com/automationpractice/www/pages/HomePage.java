package com.automationpractice.www.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

    @FindBy(id = "search_query_top")
    private WebElement searchElement;

    @FindBy(name = "submit_search")
    private WebElement searchButtonElement;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public SearchResultPage searchFor(String itemName) {
        searchElement.sendKeys(itemName);
        searchButtonElement.click();
        return PageFactory.initElements(Driver, SearchResultPage.class);
    }
}
