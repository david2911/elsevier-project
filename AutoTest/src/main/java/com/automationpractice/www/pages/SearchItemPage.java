package com.automationpractice.www.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchItemPage extends BasePage {

    @FindBy(name = "Submit")
    private WebElement addToCartButtonElement;

    public SearchItemPage(WebDriver driver) {
        super(driver);
    }

    public AddToCartPage makeSelection() {
        addToCartButtonElement.click();
        return PageFactory.initElements(Driver, AddToCartPage.class);
    }
}
