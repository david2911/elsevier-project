package com.automationpractice.www.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddToCartPage extends BasePage {

    @FindBy(css = "a.btn:nth-child(2) > span:nth-child(1)")
    private WebElement checkElement;

    public AddToCartPage(WebDriver driver) {
        super(driver);
    }

    public ShoppingCartSummaryPage doProceedToCheckout() {
        checkElement.click();
        return PageFactory.initElements(Driver, ShoppingCartSummaryPage.class);
    }
}


