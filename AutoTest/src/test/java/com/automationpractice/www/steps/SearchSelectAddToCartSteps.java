package com.automationpractice.www.steps;

import com.automationpractice.www.browserUtils.BrowserFactory;
import com.automationpractice.www.pages.BasePage;
import com.automationpractice.www.pages.HomePage;
import com.automationpractice.www.pages.UserAuthenticationPage;
import com.automationpractice.www.utilities.Props;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.core.Is;

import static org.hamcrest.MatcherAssert.assertThat;

public class SearchSelectAddToCartSteps {
    private HomePage homepage;
    private UserAuthenticationPage userAuthenticationPage;

    @Given("^I am on the homepage$")
    public void iAmOnTheHomepage() throws Throwable {
        BasePage basePage = new BasePage(BrowserFactory.getDriver());
        homepage = basePage.loadHomePage();
    }

    @When("^I select \"([^\"]*)\" and added to cart$")
    public void iSelectAndAddedToCart(String itemName) throws Throwable {
        userAuthenticationPage = homepage.searchFor(Props.getTData("item.name"))
                .previewItem()
                .makeSelection()
                .doProceedToCheckout()
                .doCheckout();
    }

    @Then("^I should be able to confirm the \"([^\"]*)\" section$")
    public void iShouldBeAbleToConfirmTheSection(String signIn) throws Throwable {
        assertThat(userAuthenticationPage.verifySignInPage(Props.getTData("sign.in")), Is.is(true));
    }
}
