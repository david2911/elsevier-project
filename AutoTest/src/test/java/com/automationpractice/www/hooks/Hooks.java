package com.automationpractice.www.hooks;

import com.automationpractice.www.browserUtils.BrowserFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    private BrowserFactory bf;

    @Before(order = 1)
    public void startTest1(){
        bf = new BrowserFactory();
        bf.initialiseBrowser();
    }
    @Before(order = 2)
    public void startTest2(){
        bf.prepareBrowser();
    }
    @After(order = 1)
    public void stopTest(){
        bf.deInitialiseBrowser();
    }
}
