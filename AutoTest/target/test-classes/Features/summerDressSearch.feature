@regression
Feature: This is a feature to validate summer dresses can be added to cart and proceed to sign in
  As a automationpractice site user
  I want to select summer dresses and add to cart
  So i can proceed to the sign in page



  Scenario: Selecting and add to Cart for summer dresses
    Given I am on the homepage
    When I select "itemName" and added to cart
    Then I should be able to confirm the "Sign In" section