$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Features/summerDressSearch.feature");
formatter.feature({
  "line": 2,
  "name": "This is a feature to validate summer dresses can be added to cart and proceed to sign in",
  "description": "As a automationpractice site user\r\nI want to select summer dresses and add to cart\r\nSo i can proceed to the sign in page",
  "id": "this-is-a-feature-to-validate-summer-dresses-can-be-added-to-cart-and-proceed-to-sign-in",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@regression"
    }
  ]
});
formatter.before({
  "duration": 4768575900,
  "status": "passed"
});
formatter.before({
  "duration": 1156673000,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Selecting and add to Cart for summer dresses",
  "description": "",
  "id": "this-is-a-feature-to-validate-summer-dresses-can-be-added-to-cart-and-proceed-to-sign-in;selecting-and-add-to-cart-for-summer-dresses",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 10,
  "name": "I am on the homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "I select \"itemName\" and added to cart",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I should be able to confirm the \"Sign In\" section",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchSelectAddToCartSteps.iAmOnTheHomepage()"
});
formatter.result({
  "duration": 2951084700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "itemName",
      "offset": 10
    }
  ],
  "location": "SearchSelectAddToCartSteps.iSelectAndAddedToCart(String)"
});
formatter.result({
  "duration": 6169807900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sign In",
      "offset": 33
    }
  ],
  "location": "SearchSelectAddToCartSteps.iShouldBeAbleToConfirmTheSection(String)"
});
formatter.result({
  "duration": 73515800,
  "status": "passed"
});
formatter.after({
  "duration": 1195345800,
  "status": "passed"
});
});